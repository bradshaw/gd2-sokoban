﻿var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");
var moveCounter = document.getElementById("moveCounter");

var movesnd = new Audio("Move.wav"); // buffers automatically when created
var scoresnd = new Audio("Score.wav"); // buffers automatically when created
var winsnd = new Audio("Win.wav"); // buffers automatically when created
var boxsnd = new Audio("Box.wav"); // buffers automatically when created
	
var unitSize = 50
var play = 0
var moves = 0
var playerX = 0
var playerY = 0
var drawPlayerX = 0
var drawPlayerY = 0

var saveButton = document.getElementById("saveButton");
var loadButton = document.getElementById("loadButton");
var levelText = document.getElementById("levelText");


var walls = [];
var snake = [];
var boxes = [];
var targets = []

var colorTarget = [
	"red",
	'blue',
	"green",
	'brown',
	"yellow",
	"cyan",
	"magenta",
	"orange",
]

var gameOver = true;

function update () {
	drawPlayerX = lerp(drawPlayerX, playerX, 0.2);
	drawPlayerY = lerp(drawPlayerY, playerY, 0.2);
	for (var i = 0; i < boxes.length; i++) {
		if ( !boxes[i].drawx ){
			boxes[i].drawx = boxes[i].x;
			boxes[i].drawy = boxes[i].y;
		}
		boxes[i].drawx = lerp(boxes[i].drawx, boxes[i].x, 0.2);
		boxes[i].drawy = lerp(boxes[i].drawy, boxes[i].y, 0.2);
	}
}

function redraw(){
	ctx.clearRect(0,0,c.width,c.height);

	ctx.fillStyle = "black";
	for (var i = 0; i < walls.length; i++) {
		ctx.fillRect(walls[i].x*unitSize, walls[i].y*unitSize, unitSize, unitSize);
	};

	for (var i = 0; i < targets.length; i++) {
		ctx.fillStyle = colorTarget[i%colorTarget.length];
		ctx.fillRect(targets[i].x*unitSize+unitSize/4, targets[i].y*unitSize+unitSize/4, unitSize/2, unitSize/2);
	};
	
	for (var i = 0; i < boxes.length; i++) {
		ctx.fillStyle = colorTarget[i%colorTarget.length];
		ctx.fillRect(boxes[i].drawx*unitSize, boxes[i].drawy*unitSize, unitSize, unitSize);
	};

	ctx.fillStyle = "darkgrey";
	ctx.fillRect(drawPlayerX*unitSize, drawPlayerY*unitSize, unitSize, unitSize);
	moveCounter.textContent = "Moves: "+moves;
}

setInterval( function () {
	update();
	redraw();
} , 1000/60 );

function lerp (a, b, n) {
	return b * n + (1-n) * a
}

window.onkeydown = function(keyEvent) {
	var xdir = 0;
	var ydir = 0;
	if (keyEvent.keyCode==37) {			// Left arrow
		xdir--;
	} else if (keyEvent.keyCode==38) {	// Up arrow
		ydir--;
	} else if (keyEvent.keyCode==39) {	// Right arrow
		xdir++;
	} else if (keyEvent.keyCode==40) {	// Down arrow
		ydir++;
	}
	move(xdir, ydir);
	if (!gameOver && checkWin()){
		winsnd.play();
		gameOver = true;
	}
}


function getBox(x,y){
	for (var i = 0; i< boxes.length;i++){
		if (x==boxes[i].x && y==boxes[i].y){
			return boxes[i];
		}
	}
}

function isWall (x,y){
	for (var i = 0; i< walls.length; i++){
		if(x==walls[i].x&&y==walls[i].y){
			return true;
		}
	}
	return false;
}

function isTarget (x,y){
	for (var i = 0; i< targets.length; i++){
		if(x==targets[i].x&&y==targets[i].y){
			return true;
		}
	}
	return false;
}



function isInScreen (x,y){//creation de conditions
		return(x>=0&&
		x<c.width/unitSize &&
		y>=0&&
		y<c.height/unitSize);
}

function createWalls (x,y){
	if (!getBox(x,y) && !isWall(x,y) && !isTarget(x,y)){
		walls.push({
			x:x,
			y:y
		});	
	}
}

function deleteWalls (x,y){
	if (isWall(x,y) ){
		for (var i=0; i<walls.length; i++){
			var wall =walls[i];
			if(wall.x==x && wall.y == y){
				walls.splice(i,1)
			}
		}
	}
	
}

function createBox(x,y){
	if (!getBox(x,y) && !isWall (x,y) && !isTarget(x,y)){
		boxes.push({
			x: x,
			y: y,
			drawx: x,
			drawy: y
		});
		 
			
	}		
}
function deleteBox (x,y){
	if (getBox(x,y) ){
		for (var i=0; i<boxes.length; i++){
			var box =boxes[i];
			if(box.x==x && box.y == y){
				boxes.splice(i,1)
			}
		}
	}
	
}

function createTarget(x,y){
	if (!getBox(x,y) && !isWall (x,y) && !isTarget(x,y)){
		targets.push({
			x: x,
			y: y
		});
	
	}		
}

function deleteTarget (x,y){
	if (isTarget(x,y) ){
		for (var i=0; i<targets.length; i++){
			var target=targets[i];
			if(target.x==x && target.y == y){
				targets.splice(i,1)
			}
		}
	}
	
}


function saveLevel (){
	levelText.value = JSON.stringify({
		walls: walls,
		playerX: playerX,
		playerY: playerY,
		boxes: boxes,
		targets: targets
		})
}

function loadLevel (){
	var level = JSON.parse(levelText.value);
	walls= level.walls;
	playerX= level.playerX
	playerY= level.playerY
	boxes= level.boxes
	targets = level.targets
}

c.onmousedown=function(mouseEvent){
	console.log(mouseEvent);
	if(mouseEvent.shiftKey){
		deleteWalls(
			Math.floor(mouseEvent.offsetX/unitSize),
			Math.floor(mouseEvent.offsetY/unitSize)
		);
		deleteBox(
			Math.floor(mouseEvent.offsetX/unitSize),
			Math.floor(mouseEvent.offsetY/unitSize)
		);
		deleteTarget(
			Math.floor(mouseEvent.offsetX/unitSize),
			Math.floor(mouseEvent.offsetY/unitSize)
		);
	} else {
		if (mouseEvent.altKey){
			createBox(
				Math.floor(mouseEvent.offsetX/unitSize),
				Math.floor(mouseEvent.offsetY/unitSize)
			);
		}else if (mouseEvent.ctrlKey){
			createTarget(
				Math.floor(mouseEvent.offsetX/unitSize),
				Math.floor(mouseEvent.offsetY/unitSize)
			);	
	
		} else {
			createWalls(
				Math.floor(mouseEvent.offsetX/unitSize),
				Math.floor(mouseEvent.offsetY/unitSize)
			);
		}
	
	}
	redraw();
	
}


saveButton.onclick = function(){
	saveLevel();
	redraw();
}

loadButton.onclick = function(){
	loadLevel();
	redraw();
}

playButton.onclick = function(){
	play = 1;
	moves = 0;
	gameOver = false;
	redraw();
}

function canMove(x,y) { 
		if (isInScreen (playerX+x,playerY+y)){
			var box = getBox(playerX+x, playerY+y);
			if (box){
				if (isInScreen(box.x+x,box.y+y)){
					return (!isWall(box.x+x,box.y+y)) && (!getBox(box.x+x,box.y+y));
				} else {
					return false;
				}
		 }
			
			return !isWall(playerX+x, playerY+y);
		}else return false
}
 
function move (x,y){
	
	if ((x!=0||y!=0) && canMove(x,y))//utiliusation des conditions
	{
		movesnd.currentTime = 0;
		movesnd.play();
		playerX += x;
		playerY += y;
		if (!gameOver){
			moves++;
		}
		var box = getBox (playerX, playerY);
		if (box){
			boxsnd.currentTime = 0;
			boxsnd.play();
			box.x+=x
			box.y+=y
		}
		
	}
}


function checkWin () {
	console.log(boxes.length);
	if (boxes.length==0 || boxes.length!=targets.length) return false; 
	for (var i = 0; i < boxes.length; i++) {
		if (boxes[i].x != targets[i].x || boxes[i].y != targets[i].y){
			console.log("Fail on: "+i);
			return false;
		}
	}
	return true;
}


window.onload=redraw;